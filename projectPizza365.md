### ProjectPizza365.md

## 1. Giải Thích Code Các Method Đã Viết

### Method: `createUserOrder`
- **Mục đích:** Tạo một order mới từ thông tin người dùng.
- **Cách hoạt động:**
  - Lấy dữ liệu từ `request body`.
  - Random một `orderCode`.
  - Kiểm tra xem người dùng đã tồn tại trong hệ thống bằng `email`.
  - Nếu người dùng đã tồn tại, lấy `id` của người dùng để tạo `Order`.
  - Nếu chưa tồn tại, tạo một người dùng mới và lấy `id` của người dùng mới đó để tạo `Order`.
  - Trả về thông tin order vừa được tạo.

### Method: `getAllOrders`
- **Mục đích:** Lấy danh sách tất cả các orders hoặc tìm order bằng `orderCode`.
- **Cách hoạt động:**
  - Nếu có `orderCode` trong query parameters, tìm order dựa trên `orderCode`.
  - Nếu không có `orderCode`, lấy tất cả các orders.
  - Trả về thông tin order hoặc danh sách tất cả orders.

### Method: `updateOrderById`
- **Mục đích:** Cập nhật thông tin order dựa trên `orderId`.
- **Cách hoạt động:**
  - Lấy `orderId` từ `request params`.
  - Lấy dữ liệu cập nhật từ `request body`.
  - Tìm order bằng `orderId` và cập nhật thông tin order.
  - Trả về thông tin order đã được cập nhật.

### Method: `getOrderByOrderCode`
- **Mục đích:** Lấy thông tin order dựa trên `orderCode`.
- **Cách hoạt động:**
  - Lấy `orderCode` từ query parameters.
  - Tìm order bằng `orderCode`.
  - Trả về thông tin order.

## 2. Mô Tả Dự Án

Project Pizza365 là một hệ thống quản lý đơn hàng và lịch sử giải thưởng cho một cửa hàng pizza. Hệ thống cho phép người dùng tạo đơn hàng mới, kiểm tra voucher, và quản lý thông tin người dùng. Các chức năng chính bao gồm tạo đơn hàng, lấy danh sách đơn hàng, và liểm tra voucher của người dùng.

## 3. Công Nghệ Sử Dụng

- **Node.js:** Môi trường runtime cho JavaScript.
- **Express.js:** Framework cho Node.js để xây dựng các API RESTful.
- **MongoDB:** Cơ sở dữ liệu NoSQL để lưu trữ thông tin người dùng, đơn hàng và lịch sử giải thưởng.
- **Mongoose:** Thư viện ODM cho MongoDB và Node.js để quản lý dữ liệu.
- **Bootstrap:** Framework CSS để thiết kế giao diện người dùng.
- **AJAX (XMLHttpRequest):** Sử dụng để giao tiếp không đồng bộ giữa client và server.

---
