const express = require('express');
const app = express();
const path = require('path'); 
const port = 8000;
const mongoose = require('mongoose');
const cors = require('cors');
const drinkRouter = require("./app/routes/drink.router");
const voucherRouter = require("./app/routes/voucher.router");
const orderRouter = require("./app/routes/order.router");
const userRouter = require("./app/routes/user.router");

// định nghĩa middleware để phục vụ các tệp tĩnh (css, hình ảnh, ...)
app.use(express.static('views'));
app.use(express.json());
app.use(cors());

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/devcamp_pizza365")
    .then(() => {
        console.log("MongoDB connected");
    })
    .catch((err) => {
        console.log(err);
    });

// định nghĩa route cho trang chủ
app.get('/', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/pizza365index.html' );
    res.sendFile(indexPath);
})

app.get('/restAPI', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/sample.restAPI.calls.html' );
    res.sendFile(indexPath);
})
// sử dụng router
app.use("/devcamp-pizza365/drinks", drinkRouter);
app.use("/devcamp-pizza365/vouchers", voucherRouter);
app.use("/devcamp-pizza365", orderRouter);
app.use("/devcamp-pizza365/users", userRouter);
// lắng nghe trên cổng 8000
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})

module.exports = app