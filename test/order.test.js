const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const mongoose = require('mongoose');
const Order = require('../app/models/order.model');
const Drink = require('../app/models/drink.model');
const Voucher = require('../app/models/voucher.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Pizza 365 - Order API", () => {
    let drinkid;
    let voucherid;

    // Ensure the database is clean before running tests
    before(async () => {
        try {
            await mongoose.connect('mongodb://127.0.0.1:27017/devcamp_pizza365', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
    
            mongoose.connection.on('error', (error) => {
                console.error('MongoDB connection error:', error);
            });
    
            console.log('MongoDB connected successfully.');
    
            await Order.deleteMany({});
            await Drink.deleteMany({});
            await Voucher.deleteMany({});
    
            const dummyDrink1 = new Drink({
                maNuocUong: "TRA002",
                tenNuocUong: "Tra tac",
                donGia: 10000
            });
            const savedDrink1 = await dummyDrink1.save();
            drinkid = savedDrink1._id;
    
            const dummyVoucher1 = new Voucher({
                maVoucher: "VOUCHER100",
                phanTramGiamGia: 20,
                ghiChu: "Discount Voucher 100"
            });
            const savedVoucher1 = await dummyVoucher1.save();
            voucherid = savedVoucher1._id;
    
            console.log('Dummy drink created:', savedDrink1);
            console.log('Dummy voucher created:', savedVoucher1);
            console.log('Drink ID:', drinkid);
            console.log('Voucher ID:', voucherid);
    
        } catch (error) {
            console.error('Error in before hook:', error);
        }
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });

    describe("POST /devcamp-pizza365/detailed-orders", () => {
        it("Should create a new order", (done) => {
            chai.request(server)
                .post("/devcamp-pizza365/detailed-orders")
                .send({
                    orderCode: "ORDER002",
                    pizzaSize: "M",
                    pizzaType: "Hawaiian",
                    drinkid: drinkid.toString(),  // Đảm bảo gửi drinkid dưới dạng chuỗi
                    voucherid: voucherid.toString(),  // Đảm bảo gửi voucherid dưới dạng chuỗi
                    status: "Completed"
                })
                .end((err, res) => {
                    if (err) {
                        console.log('POST /detailed-orders Error:', err.message);  // Log lỗi để giúp kiểm tra nguyên nhân
                        done(err);
                    } else {
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('orderCode').eql('ORDER002');
                        res.body.data.should.have.property('pizzaSize').eql('M');
                        res.body.data.should.have.property('pizzaType').eql('Hawaiian');
                        res.body.data.should.have.property('status').eql('Completed');
                        
                        // Kiểm tra phần tử đầu tiên trong mảng `drink` và `voucher` là drinkid và voucherid
                        res.body.data.should.have.property('drink').that.is.an('array').and.that.includes(drinkid.toString());
                        res.body.data.should.have.property('voucher').that.is.an('array').and.that.includes(voucherid.toString());
    
                        done();
                    }
                });
        });
    });
    
    describe("GET /devcamp-pizza365/orders", () => {
        it("Should return an array of all orders", (done) => {
            chai.request(server)
                .get("/devcamp-pizza365/orders")
                .end((err, res) => {
                    if (err) {
                        console.log('GET /orders Error:', err.message);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.data.should.be.a('array');
                        res.body.data.length.should.be.above(0);  // Ensure there is at least one order
                        res.body.data[0].should.have.property('orderCode');
                        res.body.data[0].should.have.property('pizzaSize');
                        res.body.data[0].should.have.property('pizzaType');
                        res.body.data[0].should.have.property('status');
                        res.body.data[0].should.have.property('drink').that.is.an('array');  // Ensure `drink` exists and is an array
                        res.body.data[0].should.have.property('voucher').that.is.an('array');  // Ensure `voucher` exists and is an array
                        done();
                    }
                });
        });
    });

    describe("GET /devcamp-pizza365/orders/:orderid", () => {
        let orderid;

        before(async () => {
            // Create an order for this test
            const order = new Order({
                orderCode: "ORDER004",
                pizzaSize: "M",
                pizzaType: "Veggie",
                voucher: [voucherid],
                drink: [drinkid],
                status: "Pending"
            });
            const savedOrder = await order.save();
            orderid = savedOrder._id;  // Đảm bảo rằng `orderid` là ID của order đã được lưu
        });

        it("Should return an order by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-pizza365/orders/${orderid}`)
                .end((err, res) => {
                    if (err) {
                        console.log('GET /orders/:orderid Error:', err.message);  // Log lỗi để giúp kiểm tra nguyên nhân
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('orderCode').eql('ORDER004');
                        res.body.data.should.have.property('pizzaSize').eql('M');
                        res.body.data.should.have.property('pizzaType').eql('Veggie');
                        res.body.data.should.have.property('status').eql('Pending');
                        res.body.data.should.have.property('drink').that.includes(drinkid.toString());  // Kiểm tra ID drink
                        res.body.data.should.have.property('voucher').that.includes(voucherid.toString());  // Kiểm tra ID voucher
                        done();
                    }
                });
        });
    });

    describe("PUT /devcamp-pizza365/orders/:orderid", () => {
        let orderid;

        before(async () => {
            // Create an order for this test
            const order = new Order({
                orderCode: "ORDER005",
                pizzaSize: "L",
                pizzaType: "Pepperoni",
                voucher: [voucherid],
                drink: [drinkid],
                status: "Pending"
            });
            const savedOrder = await order.save();
            orderid = savedOrder._id;  // Đảm bảo rằng `orderid` là ID của order đã được lưu
        });

        it("Should update an order by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-pizza365/orders/${orderid}`)
                .send({
                    pizzaSize: "M",
                    status: "Completed"
                })
                .end((err, res) => {
                    if (err) {
                        console.log('PUT /orders/:orderid Error:', err.message);  // Log lỗi để giúp kiểm tra nguyên nhân
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('data');
                        res.body.data.should.have.property('pizzaSize').eql('M');
                        res.body.data.should.have.property('status').eql('Completed');
                        done();
                    }
                });
        });
    });

    describe("DELETE /devcamp-pizza365/orders/:orderid", () => {
        let orderid;

        before(async () => {
            // Create an order for this test
            const order = new Order({
                orderCode: "ORDER006",
                pizzaSize: "M",
                pizzaType: "Hawaiian",
                voucher: [voucherid],
                drink: [drinkid],
                status: "Pending"
            });
            const savedOrder = await order.save();
            orderid = savedOrder._id;  // Đảm bảo rằng `orderid` là ID của order đã được lưu
        });

        it("Should delete an order by ID", (done) => {
            chai.request(server)
                .delete(`/devcamp-pizza365/orders/${orderid}`)
                .end(async (err, res) => {
                    if (err) {
                        console.log('DELETE /orders/:orderid Error:', err.message);  // Log lỗi để giúp kiểm tra nguyên nhân
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Xóa thông tin order thành công');

                        // Verify that the order was actually deleted
                        const order = await Order.findById(orderid);
                        should.not.exist(order);

                        done();
                    }
                });
        });
    });
});
