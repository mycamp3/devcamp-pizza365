const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const mongoose = require('mongoose');
const Voucher = require('../app/models/voucher.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Pizza 365 - Voucher API", () => {
    // Ensure the database is clean before running tests
    before(async () => {
        await mongoose.connect('mongodb://127.0.0.1:27017/devcamp_pizza365');
        mongoose.connection.on('error', (e) => {
            if (e.message.code === 'ETIMEDOUT') {
                console.log(e);
                mongoose.connect('mongodb://127.0.0.1:27017/devcamp_pizza365');
            }
            console.log(e);
        });

        // Clear the collection before each test
        await Voucher.deleteMany({});
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });

    describe("POST /devcamp-pizza365/vouchers", () => {
    it("Should create a new voucher", (done) => {
        chai.request(server)
            .post("/devcamp-pizza365/vouchers")
            .send({
                reqMaVoucher: "VOUCHER001",
                reqGhiChu: "Discount Voucher",
                reqPhanTramGiamGia: 10
            })
            .end((err, res) => {
                if (err) done(err);
                
                // Add console log to debug response body
                console.log(res.body);

                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('data');
                res.body.data.should.have.property('maVoucher').eql('VOUCHER001');
                res.body.data.should.have.property('ghiChu').eql('Discount Voucher');
                res.body.data.should.have.property('phanTramGiamGia').eql(10);
                done();
            });
    });
});

    describe("GET /devcamp-pizza365/vouchers", () => {
        it("Should return an array of all vouchers", (done) => {
            chai.request(server)
                .get("/devcamp-pizza365/vouchers")
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data').which.is.a('array');
                    done();
                });
        });
    });

    describe("GET /devcamp-pizza365/vouchers/:voucherid", () => {
        let voucherid;

        before(async () => {
            const voucher = new Voucher({
                maVoucher: "VOUCHER002",
                ghiChu: "Holiday Discount",
                phanTramGiamGia: 15
            });
            const savedVoucher = await voucher.save();
            voucherid = savedVoucher._id;
        });

        it("Should return a voucher by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-pizza365/vouchers/${voucherid}`)
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('maVoucher').eql('VOUCHER002');
                    res.body.data.should.have.property('ghiChu').eql('Holiday Discount');
                    res.body.data.should.have.property('phanTramGiamGia').eql(15);
                    done();
                });
        });
    });

    describe("PUT /devcamp-pizza365/vouchers/:voucherid", () => {
        let voucherid;

        before(async () => {
            const voucher = new Voucher({
                maVoucher: "VOUCHER003",
                ghiChu: "New Year Discount",
                phanTramGiamGia: 20
            });
            const savedVoucher = await voucher.save();
            voucherid = savedVoucher._id;
        });

        it("Should update a voucher by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-pizza365/vouchers/${voucherid}`)
                .send({
                    reqMaVoucher: "VOUCHER003",
                    reqGhiChu: "Updated New Year Discount",
                    reqPhanTramGiamGia: 25
                })
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('maVoucher').eql('VOUCHER003');
                    res.body.data.should.have.property('ghiChu').eql('Updated New Year Discount');
                    res.body.data.should.have.property('phanTramGiamGia').eql(25);
                    done();
                });
        });
    });

    describe("DELETE /devcamp-pizza365/vouchers/:voucherid", () => {
        let voucherid;

        before(async () => {
            const voucher = new Voucher({
                maVoucher: "VOUCHER004",
                ghiChu: "Summer Discount",
                phanTramGiamGia: 30
            });
            const savedVoucher = await voucher.save();
            voucherid = savedVoucher._id;
        });

        it("Should delete a voucher by ID", (done) => {
            chai.request(server)
                .delete(`/devcamp-pizza365/vouchers/${voucherid}`)
                .end(async (err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Xóa thông tin voucher thành công');

                    // Verify that the voucher was actually deleted
                    const voucher = await Voucher.findById(voucherid);
                    should.not.exist(voucher);

                    done();
                });
        });
    });
});
