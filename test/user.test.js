const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const mongoose = require('mongoose');
const User = require('../app/models/user.model');
const Order = require('../app/models/order.model');
const Drink = require('../app/models/drink.model');
const Voucher = require('../app/models/voucher.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Pizza 365 - User API", () => {
    let orderid;
    let drinkid;
    let voucherid;

    // Ensure the database is clean before running tests
    before(async () => {
        await mongoose.connect('mongodb://127.0.0.1:27017/devcamp_pizza365'); 
        mongoose.connection.on('error', (e) => {
            if (e.message.code === 'ETIMEDOUT') {
                console.log(e);
                mongoose.connect('mongodb://127.0.0.1:27017/devcamp_pizza365');
            }
            console.log(e);
        });

        // Clear the collections before each test
        await User.deleteMany({});
        await Order.deleteMany({});
        await Drink.deleteMany({});
        await Voucher.deleteMany({});

        // Create a dummy drink
        const dummyDrink = new Drink({
            maNuocUong: "COKE001",
            tenNuocUong: "Coke",
            donGia: 10000
        });
        const savedDrink = await dummyDrink.save();
        drinkid = savedDrink._id;

        // Create a dummy voucher
        const dummyVoucher = new Voucher({
            maVoucher: "VOUCHER001",
            phanTramGiamGia: 10,
            ghiChu: "Discount Voucher"
        });
        const savedVoucher = await dummyVoucher.save();
        voucherid = savedVoucher._id;

        // Create a dummy order
        const dummyOrder = new Order({
            orderCode: "ORDER001",
            pizzaSize: "L",
            pizzaType: "Pepperoni",
            voucher: [voucherid],
            drink: [drinkid],
            status: "Pending"
        });
        const savedOrder = await dummyOrder.save();
        orderid = savedOrder._id;
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });

    describe("POST /devcamp-pizza365/users", () => {
        it("Should create a new user", (done) => {
            chai.request(server)
                .post("/devcamp-pizza365/users")
                .send({
                    fullName: "John Doe",
                    email: "john.doe@example.com",
                    address: "123 Main St",
                    phone: "1234567890",
                    orderid: orderid
                })
                .end((err, res) => {
                    if (err) done(err);
                    
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('fullName').eql('John Doe');
                    res.body.data.should.have.property('email').eql('john.doe@example.com');
                    res.body.data.should.have.property('address').eql('123 Main St');
                    res.body.data.should.have.property('phone').eql('1234567890');
                    res.body.data.should.have.property('order').which.includes(orderid.toString());
                    done();
                });
        });
    });

    describe("GET /devcamp-pizza365/users", () => {
        it("Should return an array of all users", (done) => {
            chai.request(server)
                .get("/devcamp-pizza365/users")
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data').which.is.a('array');
                    res.body.data[0].should.have.property('order').which.includes(orderid.toString());
                    done();
                });
        });
    });

    describe("GET /devcamp-pizza365/users/:userid", () => {
        let userid;

        before(async () => {
            const user = new User({
                fullName: "Jane Doe",
                email: "jane.doe@example.com",
                address: "456 Main St",
                phone: "0987654321",
                order: [orderid]
            });
            const savedUser = await user.save();
            userid = savedUser._id;
        });

        it("Should return a user by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-pizza365/users/${userid}`)
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('fullName').eql('Jane Doe');
                    res.body.data.should.have.property('order').which.includes(orderid.toString());
                    done();
                });
        });
    });

    describe("PUT /devcamp-pizza365/users/:userid", () => {
        let userid;

        before(async () => {
            const user = new User({
                fullName: "Mark Smith",
                email: "mark.smith@example.com",
                address: "789 Main St",
                phone: "1122334455",
                order: [orderid]
            });
            const savedUser = await user.save();
            userid = savedUser._id;
        });

        it("Should update a user by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-pizza365/users/${userid}`)
                .send({
                    fullName: "Mark Smith Updated",
                    email: "mark.smith.updated@example.com",
                    address: "789 Updated St",
                    phone: "5566778899",
                    orderid: orderid
                })
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('fullName').eql('Mark Smith Updated');
                    res.body.data.should.have.property('order').which.includes(orderid.toString());
                    done();
                });
        });
    });

    describe("DELETE /devcamp-pizza365/users/:userid", () => {
        let userid;

        before(async () => {
            const user = new User({
                fullName: "Lisa White",
                email: "lisa.white@example.com",
                address: "321 Main St",
                phone: "6677889900",
                order: [orderid]
            });
            const savedUser = await user.save();
            userid = savedUser._id;
        });

        it("Should delete a user by ID", (done) => {
            chai.request(server)
                .delete(`/devcamp-pizza365/users/${userid}`)
                .end(async (err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Xóa người dùng thành công');

                    // Verify that the user was actually deleted
                    const user = await User.findById(userid);
                    should.not.exist(user);

                    done();
                });
        });
    });
});
