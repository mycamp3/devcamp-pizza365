const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const mongoose = require('mongoose');
const Drink = require('../app/models/drink.model');

const should = chai.should();

chai.use(chaiHttp);

describe("Test Devcamp Pizza 365 - Drink API", () => {
    // Ensure the database is clean before running tests
    before(async () => {
        await mongoose.connect('mongodb://127.0.0.1:27017/devcamp_pizza365');
        mongoose.connection.on('error', (e) => {
            if (e.message.code === 'ETIMEDOUT') {
                console.log(e);
                mongoose.connect('mongodb://127.0.0.1:27017/devcamp_pizza365');
            }
            console.log(e);
        });

        // Clear the collection before each test
        await Drink.deleteMany({});
    });

    after(async () => {
        // Disconnect from the database after all tests
        await mongoose.connection.close();
    });
    
    describe("POST /devcamp-pizza365/drinks", () => {
        it("Should create a new drink", (done) => {
            chai.request(server)
                .post("/devcamp-pizza365/drinks")
                .send({
                    "reqMaNuocUong": "004",
                    "reqTenNuocUong": "Fanta",
                    "reqDonGia": 10000
                })
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('maNuocUong').eql('004');
                    res.body.data.should.have.property('tenNuocUong').eql('Fanta');
                    res.body.data.should.have.property('donGia').eql(10000);
                    done();
                });
        });
    });
    describe("Test get all drink", () => {
        it("Should return an array of all drinks", (done) => {
            chai.request(server)
                .get("/devcamp-pizza365/drinks")
                .end((err, res) => {
                    if (err) done(err);
                    console.log(res.body);
                    res.should.have.status(200);
                    res.body.data.should.be.a('array');
                    done();
                });
        });
    });
    describe("GET /devcamp-pizza365/drinks/:drinkid", () => {
        let drinkid;

        before(async () => {
            const drink = new Drink({
                maNuocUong: "002",
                tenNuocUong: "7UP",
                donGia: 12000
            });
            const savedDrink = await drink.save();
            drinkid = savedDrink._id;
        });

        it("Should return a drink by ID", (done) => {
            chai.request(server)
                .get(`/devcamp-pizza365/drinks/${drinkid}`)
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('maNuocUong').eql('002');
                    res.body.data.should.have.property('tenNuocUong').eql('7UP');
                    res.body.data.should.have.property('donGia').eql(12000);
                    done();
                });
        });
    });

    describe("PUT /devcamp-pizza365/drinks/:drinkid", () => {
        let drinkid;
    
        before(async () => {
            const drink = new Drink({
                maNuocUong: "003",
                tenNuocUong: "Mirinda",
                donGia: 15000
            });
            const savedDrink = await drink.save();
            drinkid = savedDrink._id;
        });
    
        it("Should update a drink by ID", (done) => {
            chai.request(server)
                .put(`/devcamp-pizza365/drinks/${drinkid}`)
                .send({
                    reqMaNuocUong: "003",
                    reqTenNuocUong: "Mirinda Orange",
                    reqDonGia: 16000
                })
                .end((err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    res.body.data.should.have.property('maNuocUong').eql('003');
                    res.body.data.should.have.property('tenNuocUong').eql('Mirinda Orange');
                    res.body.data.should.have.property('donGia').eql(16000);
                    done();
                });
        });
    });
    

    describe("DELETE /devcamp-pizza365/drinks/:drinkid", () => {
        let drinkid;

        before(async () => {
            const drink = new Drink({
                "maNuocUong": "005",
                "tenNuocUong": "Sprite",
                "donGia": 11000
            });
            const savedDrink = await drink.save();
            drinkid = savedDrink._id;
        });

        it("Should delete a drink by ID", (done) => {
            chai.request(server)
                .delete(`/devcamp-pizza365/drinks/${drinkid}`)
                .end(async (err, res) => {
                    if (err) done(err);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Drink deleted successfully');

                    // Verify that the drink was actually deleted
                    const drink = await Drink.findById(drinkid);
                    should.not.exist(drink);

                    done();
                });
        });
    });
});
