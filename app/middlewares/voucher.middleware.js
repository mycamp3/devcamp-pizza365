const getAllVoucherMiddleware = (req, res, next) => {
    console.log("GET all voucher middleware");
    
    next();
}

const createVoucherMiddleware = (req, res, next) => {
    console.log("POST voucher middleware");
    
    next();
}
const getVoucherByIdMiddleware = (req, res, next) => {
    console.log("GET voucher by id middleware");
    
    next();
}
const updateVoucherMiddleware = (req, res, next) => {
    console.log("UPDATE voucher middleware");
    
    next();
}
const deleteVoucherMiddleware = (req, res, next) => {
    console.log("DELETE voucher middleware");
    
    next();
}
module.exports = {
    getAllVoucherMiddleware,
    createVoucherMiddleware,
    getVoucherByIdMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
}