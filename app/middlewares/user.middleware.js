const getAllUserMiddleware = (req, res, next) => {
    console.log("GET all Users middleware");
    
    next();
}

const createUserMiddleware = (req, res, next) => {
    console.log("POST User middleware");
    
    next();
}
const getUserByIdMiddleware = (req, res, next) => {
    console.log("GET User by id middleware");
    
    next();
}
const updateUserMiddleware = (req, res, next) => {
    console.log("UPDATE User middleware");
    
    next();
}
const deleteUserMiddleware = (req, res, next) => {
    console.log("DELETE User middleware");
    
    next();
}
module.exports = {
    getAllUserMiddleware,
    createUserMiddleware,
    getUserByIdMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}