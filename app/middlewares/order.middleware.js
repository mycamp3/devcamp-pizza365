const getAllOrderMiddleware = (req, res, next) => {
    console.log("GET all orders middleware");
    
    next();
}

const createOrderMiddleware = (req, res, next) => {
    console.log("POST order middleware");
    
    next();
}
const getOrderByIdMiddleware = (req, res, next) => {
    console.log("GET order by id middleware");
    
    next();
}
const updateOrderMiddleware = (req, res, next) => {
    console.log("UPDATE order middleware");
    
    next();
}
const deleteOrderMiddleware = (req, res, next) => {
    console.log("DELETE order middleware");
    
    next();
}
module.exports = {
    getAllOrderMiddleware,
    createOrderMiddleware,
    getOrderByIdMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}