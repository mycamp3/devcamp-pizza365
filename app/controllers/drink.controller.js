const drinkModel = require("../models/drink.model");
const mongoose = require("mongoose");

const createDrink = async (req, res) => {
    //B1: Thu thập DL

    const {
        reqMaNuocUong,
        reqTenNuocUong,
        reqDonGia
    } = req.body
    console.log(req.body);
    //B2: Validate Dl
    if(!reqMaNuocUong) {
        return res.status(400).json({
            message: "Mã nước uống ko hợp lệ"
        })
    }

    if(!reqTenNuocUong) {
        return res.status(400).json({
            message: "Tên nước uống ko hợp lệ"
        })
    }

    if(reqDonGia < 0) {
        return res.status(400).json({
            message: "Đơn giá ko hợp lệ"
        })
    }

    try {
        // B3: Xl dữ liệu

        var newDrink = {
            maNuocUong: reqMaNuocUong,
            tenNuocUong: reqTenNuocUong,
            donGia: reqDonGia
        }
        const result = await drinkModel.create(newDrink);

        return res.status(201).json({
            message: "Tạo thành công",
            data: result
        })
    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const getAllDrinks = async (req,res) => {
    // B1:
    // B2:
    // B3: 
    try {
        const result = await drinkModel.find();

        return res.status(200).json({
            message: "Lấy ds drinks thành công",
            data: result
        })
    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const getDrinkById = async (req,res) => {
    // B1: Thu thập dl
    const drinkid = req.params.drinkid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            message: "Drinkid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await drinkModel.findById(drinkid);

        if(result) {
            return res.status(200).json({
                message: "Lấy thông tin drink thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin drink"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const updateDrinkById = async (req, res) => {
    const drinkid = req.params.drinkid;
    const {
        reqMaNuocUong,
        reqTenNuocUong,
        reqDonGia
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            message: "Drinkid không hợp lệ"
        });
    }

    if (!reqMaNuocUong) {
        return res.status(400).json({
            message: "Mã nước uống không hợp lệ"
        });
    }

    if (!reqTenNuocUong) {
        return res.status(400).json({
            message: "Tên nước uống không hợp lệ"
        });
    }

    if (reqDonGia < 0) {
        return res.status(400).json({
            message: "Đơn giá không hợp lệ"
        });
    }

    try {
        const newUpdateDrink = {
            maNuocUong: reqMaNuocUong,
            tenNuocUong: reqTenNuocUong,
            donGia: reqDonGia
        };

        const result = await drinkModel.findByIdAndUpdate(drinkid, newUpdateDrink, { new: true });

        if (result) {
            return res.status(200).json({
                message: "Update thông tin drink thành công",
                data: result
            });
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin drink"
            });
        }

    } catch (error) {
        console.error(error);
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        });
    }
};


const deleteDrinkById = async (req,res) => {
    // B1: Thu thập dl
    const drinkid = req.params.drinkid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            message: "Drinkid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await drinkModel.findByIdAndDelete(drinkid);

        if(result) {
            return res.status(200).json({
                message: "Drink deleted successfully",
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin drink"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

module.exports = {
    createDrink,
    getAllDrinks,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
}