const mongoose = require('mongoose');
const userModel = require('../models/user.model'); // Đường dẫn tới model User
const orderModel = require('../models/order.model'); // Đường dẫn tới model Order

// Tạo mới User
const createUser = async (req, res) => {
    const { fullName, email, address, phone, orderid } = req.body;

    // Validate dữ liệu đầu vào
    if (!fullName || !email || !address || !phone) {
        return res.status(400).json({
            message: "Thông tin không hợp lệ"
        });
    }

    try {
        // Kiểm tra sự tồn tại của các order
        if (orderid && !mongoose.Types.ObjectId.isValid(orderid)) {
            return res.status(400).json({
                message: "Order ID không hợp lệ"
            });
        }
        const order = await orderModel.findById(orderid);
        const newUser = new userModel({ fullName, email, address, phone, order:[order._id]});
        const savedUser = await newUser.save();

        return res.status(201).json({
            message: "Tạo người dùng thành công",
            data: savedUser
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Lấy tất cả Users
const getAllUsers = async (req, res) => {
    try {
        const users = await userModel.find();

        return res.status(200).json({
            message: "Lấy tất cả người dùng thành công",
            data: users
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Lấy User theo ID
const getUserById = async (req, res) => {
    const userid = req.params.userid;

    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "ID không hợp lệ"
        });
    }

    try {
        const user = await userModel.findById(userid);

        if (!user) {
            return res.status(404).json({
                message: "Người dùng không tồn tại"
            });
        }

        return res.status(200).json({
            message: "Lấy người dùng thành công",
            data: user
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Cập nhật User theo ID
const updateUserById = async (req, res) => {
    const userid = req.params.userid;
    const { fullName, email, address, phone, orderid } = req.body;

    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        });
    }

    try {
        const existingUser = await userModel.findById(userid);

        if (!existingUser) {
            return res.status(404).json({
                message: "User không tồn tại"
            });
        }
        // Kiểm tra sự tồn tại của các order

        let order;
        if (orderid) {
            order = await orderModel.findById(orderid);
            if (!order) {
                return res.status(404).json({ error: 'order không tồn tại' });
            }
        }
        
        existingUser.fullName = fullName || existingUser.fullName;
        existingUser.email = email || existingUser.email;
        existingUser.address = address || existingUser.address;
        existingUser.phone = phone || existingUser.phone;

        if (orderid) existingUser.order = [order._id];
        

        const updatedUser = await userModel.findByIdAndUpdate(userid, existingUser, {new: true});

        if (!updatedUser) {
            return res.status(404).json({
                message: "Người dùng không tồn tại"
            });
        }

        return res.status(200).json({
            message: "Cập nhật người dùng thành công",
            data: updatedUser
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Xóa User theo ID
const deleteUserById = async (req, res) => {
    const userid = req.params.userid;

    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "ID không hợp lệ"
        });
    }

    try {
        const deletedUser = await userModel.findByIdAndDelete(userid);

        if (!deletedUser) {
            return res.status(404).json({
                message: "Người dùng không tồn tại"
            });
        }

        return res.status(200).json({
            message: "Xóa người dùng thành công",
            data: deletedUser
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Lấy tất cả Users với limit
const getAllUsersLimit = async (req, res) => {
    try {
        const limit = parseInt(req.query.limit) || 0;
        const users = await userModel.find().limit(limit);

        return res.status(200).json({
            message: "Lấy tất cả người dùng thành công",
            data: users
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Lấy tất cả Users với skip
const getAllUsersSkip = async (req, res) => {
    try {
        const skip = parseInt(req.query.skip) || 0;
        const users = await userModel.find().skip(skip);

        return res.status(200).json({
            message: "Lấy tất cả người dùng thành công",
            data: users
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Lấy tất cả Users sắp xếp theo fullname
const getAllUsersSorted = async (req, res) => {
    try {
        const users = await userModel.find().sort({ fullName: 'asc' });

        return res.status(200).json({
            message: "Lấy tất cả người dùng thành công",
            data: users
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Lấy tất cả Users với skip và limit
const getAllUsersSkipLimit = async (req, res) => {
    try {
        const skip = parseInt(req.query.skip) || 0;
        const limit = parseInt(req.query.limit) || 0;
        const users = await userModel.find().skip(skip).limit(limit);

        return res.status(200).json({
            message: "Lấy tất cả người dùng thành công",
            data: users
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Lấy tất cả Users sắp xếp theo fullname với skip và limit
const getAllUsersSortedSkipLimit = async (req, res) => {
    try {
        const skip = parseInt(req.query.skip) || 0;
        const limit = parseInt(req.query.limit) || 0;
        const users = await userModel.find().sort({ fullName: 'asc' }).skip(skip).limit(limit);

        return res.status(200).json({
            message: "Lấy tất cả người dùng thành công",
            data: users
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error);
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
};

// Other functions like getUserById, updateUserById, deleteUserById remain unchanged

module.exports = {
    createUser,
    getAllUsers,
    getAllUsersLimit,
    getAllUsersSkip,
    getAllUsersSorted,
    getAllUsersSkipLimit,
    getAllUsersSortedSkipLimit,
    getUserById,
    updateUserById,
    deleteUserById
};
