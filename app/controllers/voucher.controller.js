const voucherModel = require("../models/voucher.model");
const mongoose = require("mongoose");

const createVoucher = async (req, res) => {
    //B1: Thu thập DL

    const {
        reqMaVoucher,
        reqGhiChu,
        reqPhanTramGiamGia
    } = req.body
    console.log(req.body);
    //B2: Validate Dl
    if(!reqMaVoucher) {
        return res.status(400).json({
            message: "Mã voucher ko hợp lệ"
        })
    }

    if(reqPhanTramGiamGia < 0) {
        return res.status(400).json({
            message: "Phần trăm giảm giá ko hợp lệ"
        })
    }

    try {
        // B3: Xl dữ liệu

        var newVoucher = {
            maVoucher: reqMaVoucher,
            ghiChu: reqGhiChu,
            phanTramGiamGia: reqPhanTramGiamGia
        }
        const result = await voucherModel.create(newVoucher);

        return res.status(201).json({
            message: "Tạo thành công",
            data: result
        })
    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const getAllVouchers = async (req,res) => {
    // B1:
    // B2:
    // B3: 
    try {
        const result = await voucherModel.find();

        return res.status(200).json({
            message: "Lấy ds vouchers thành công",
            data: result
        })
    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const getVoucherById = async (req,res) => {
    // B1: Thu thập dl
    const voucherid = req.params.voucherid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            message: "Voucherid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await voucherModel.findById(voucherid);

        if(result) {
            return res.status(200).json({
                message: "Lấy thông tin voucher thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin voucher"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const updateVoucherById = async (req,res) => {
    // B1: Thu thập dl
    const voucherid = req.params.voucherid;
    const {
        reqMaVoucher,
        reqGhiChu,
        reqPhanTramGiamGia
    } = req.body;

    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            message: "Voucherid ko hợp lệ"
        }) 
    }

    if(!reqMaVoucher) {
        return res.status(400).json({
            message: "Mã voucher ko hợp lệ"
        })
    }

    if(reqPhanTramGiamGia < 0) {
        return res.status(400).json({
            message: "Phần trăm giảm giá ko hợp lệ"
        })
    }

    // B3: Xl dl
    try {
        var newUpdateVoucher = {};
        if (reqMaVoucher) {
            newUpdateVoucher.maVoucher = reqMaVoucher
        }
        if (reqGhiChu) {
            newUpdateVoucher.ghiChu = reqGhiChu
        }
        if (reqPhanTramGiamGia) {
            newUpdateVoucher.phanTramGiamGia = reqPhanTramGiamGia
        }

        const result = await voucherModel.findByIdAndUpdate(voucherid, newUpdateVoucher,{new: true});

        if(result) {
            return res.status(200).json({
                message: "Update thông tin voucher thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin voucher"
            })
        }

    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const deleteVoucherById = async (req,res) => {
    // B1: Thu thập dl
    const voucherid = req.params.voucherid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            message: "Voucherid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await voucherModel.findByIdAndDelete(voucherid);

        if(result) {
            return res.status(200).json({
                message: "Xóa thông tin voucher thành công",
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin voucher"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

module.exports = {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}