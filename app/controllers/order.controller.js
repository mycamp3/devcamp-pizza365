const orderModel = require("../models/order.model");
const mongoose = require("mongoose");
const drinkModel = require("../models/drink.model");
const voucherModel = require("../models/voucher.model");
const userModel = require("../models/user.model");

// Function to generate a random order code
const generateRandomOrderCode = () => {
    return 'ORDER-' + Math.random().toString(36).substring(2, 9).toUpperCase();
};

const createUserOrder = async (req, res) => {
    try {
        const { fullName, email, address, phone, pizzaSize, pizzaType, status, tenNuocUong, maVoucher } = req.body;

        // Find or create user
        let user = await userModel.findOne({ email });
        if (!user) {
            user = new userModel({ fullName, email, address, phone });
            await user.save();
        }

        // Generate a random order code
        const orderCode = generateRandomOrderCode();

        // Find drink by tenNuocUong
        let drink = await drinkModel.findOne({ tenNuocUong });

        // Find voucher by maVoucher
        let voucher = await voucherModel.findOne({ maVoucher });

        // Create new order using random order code and provided details
        const newOrder = new orderModel({
            orderCode: orderCode,
            pizzaSize,
            pizzaType,
            status,
            user: user._id,
            drink: drink ? [drink._id] : [],
            voucher: voucher ? [voucher._id] : []
        });

        await newOrder.save();

        // Populate the order with drink and voucher details
        const populatedOrder = await orderModel.findById(newOrder._id)
            .populate('drink')
            .populate('voucher');

        res.status(201).json({
            message: 'Order created successfully',
            data: populatedOrder
        });
    } catch (error) {
        console.error('Error creating order:', error);
        res.status(500).json({ message: 'Internal Server Error', error });
    }
}; 
const createDetailedOrder = async (req, res) => {
    const {
        drinkid,
        voucherid,
        orderCode,
        pizzaSize,
        pizzaType,
        status
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkid not valid"
        });
    }

    if (!mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherid not valid"
        });
    }

    if (!orderCode) {
        return res.status(400).json({
            message: "Order code ko hợp lệ"
        });
    }

    if (pizzaSize !== "S" && pizzaSize !== "M" && pizzaSize !== "L") {
        return res.status(400).json({
            message: "Pizza size ko hợp lệ"
        });
    }

    if (!pizzaType) {
        return res.status(400).json({
            message: "Pizza type ko hợp lệ"
        });
    }

    if (!status) {
        return res.status(400).json({
            message: "Status không hợp lệ"
        });
    }

    try {
        const voucher = await voucherModel.findById(voucherid);
        const drink = await drinkModel.findById(drinkid);

        if (!voucher) {
            return res.status(404).json({ error: 'Voucher not found' });
        }
        if (!drink) {
            return res.status(404).json({ error: 'Drink not found' });
        }

        const newOrder = {
            voucher: [voucher._id],
            drink: [drink._id],
            orderCode: orderCode,
            pizzaSize: pizzaSize,
            pizzaType: pizzaType,
            status: status
        };

        const createdOrder = await orderModel.create(newOrder);

        return res.status(201).json({
            message: "Tạo thành công",
            data: createdOrder
        });
    } catch (error) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message
        });
    }
}




const getAllOrders = async (req, res) => {
    try {
        let condition = {};

        // Lấy orderCode từ query nếu có
        const orderCode = req.query.orderCode;

        if (orderCode) {
            // Nếu có orderCode, tìm order theo orderCode
            const order = await orderModel.findOne({ orderCode: orderCode });

            if (order) {
                return res.status(200).json({
                    message: 'Order found successfully',
                    data: order
                });
            } else {
                return res.status(404).json({
                    message: 'No order found for this orderCode',
                    data: []
                });
            }
        } else {
            // Nếu không có orderCode, lấy tất cả orders
            const orders = await orderModel.find();

            return res.status(200).json({
                message: 'Get all orders successfully',
                data: orders
            });
        }
    } catch (error) {
        console.error('Error getting orders:', error);
        res.status(500).json({ message: 'Error getting orders', error });
    }
};

const getOrderById = async (req,res) => {
    // B1: Thu thập dl
    const orderid = req.params.orderid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(orderid)) {
        return res.status(400).json({
            message: "Orderid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await orderModel.findById(orderid);

        if(result) {
            return res.status(200).json({
                message: "Lấy thông tin Order thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin Order"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const updateOrderById = async (req, res) => {
    const orderid = req.params.orderid;
    const {
        drinkid,
        voucherid,
        orderCode,
        pizzaSize,
        pizzaType,
        status
    } = req.body;

    // B2: Validate
    if (!mongoose.Types.ObjectId.isValid(orderid)) {
        return res.status(400).json({
            message: "ID Order không hợp lệ"
        });
    }

    if (drinkid && !mongoose.Types.ObjectId.isValid(drinkid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkid không hợp lệ"
        });
    }

    if (voucherid && !mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherid không hợp lệ"
        });
    }

    if (orderCode && !orderCode.trim()) {
        return res.status(400).json({
            message: "Order code không hợp lệ"
        });
    }

    if (pizzaSize && pizzaSize !== "S" && pizzaSize !== "M" && pizzaSize !== "L") {
        return res.status(400).json({
            message: "Pizza size không hợp lệ"
        });
    }

    if (pizzaType && !pizzaType.trim()) {
        return res.status(400).json({
            message: "Pizza type không hợp lệ"
        });
    }

    if (status && !status.trim()) {
        return res.status(400).json({
            message: "Status không hợp lệ"
        });
    }

    try {
        const existingOrder = await orderModel.findById(orderid);

        if (!existingOrder) {
            return res.status(404).json({
                message: "Order không tồn tại"
            });
        }

        // Kiểm tra sự tồn tại của voucher và drink
        let voucher, drink;
        if (voucherid) {
            voucher = await voucherModel.findById(voucherid);
            if (!voucher) {
                return res.status(404).json({ error: 'Voucher không tồn tại' });
            }
        }

        if (drinkid) {
            drink = await drinkModel.findById(drinkid);
            if (!drink) {
                return res.status(404).json({ error: 'Drink không tồn tại' });
            }
        }

        // Cập nhật order
        existingOrder.orderCode = orderCode || existingOrder.orderCode;
        existingOrder.pizzaSize = pizzaSize || existingOrder.pizzaSize;
        existingOrder.pizzaType = pizzaType || existingOrder.pizzaType;
        existingOrder.status = status || existingOrder.status;
        if (voucherid) existingOrder.voucher = [voucher._id];
        if (drinkid) existingOrder.drink = [drink._id];


        // Populate dữ liệu của voucher và drink
        const populatedOrder = await orderModel.findByIdAndUpdate(existingOrder._id, existingOrder, {new: true});

        return res.status(200).json({
            message: "Cập nhật thành công",
            data: populatedOrder
        });
    } catch (error) {
        console.error("Lỗi xảy ra:", error); // Ghi log lỗi chi tiết
        return res.status(500).json({
            message: "Internal Server Error",
            error: error.message // Trả về thông điệp lỗi chi tiết
        });
    }
};

const deleteOrderById = async (req,res) => {
    // B1: Thu thập dl
    const orderid = req.params.orderid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(orderid)) {
        return res.status(400).json({
            message: "Orderid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await orderModel.findByIdAndDelete(orderid);

        if(result) {
            return res.status(200).json({
                message: "Xóa thông tin order thành công",
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin order"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

module.exports = {
    createDetailedOrder,
    getAllOrders,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    createUserOrder
}