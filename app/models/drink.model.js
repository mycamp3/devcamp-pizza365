const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const drinkSchema = new Schema({
    maNuocUong: {
        type: String,
        requires: true,
        unique: true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia: {
        type: Number,
        required: false
    },
},{
    timestamps: true
});

module.exports = mongoose.model("Drink", drinkSchema);