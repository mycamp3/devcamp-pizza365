const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    order: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Order"
        }
    ],
}, {
    timestamps: true
});

module.exports = mongoose.model("User", userSchema);