const express = require("express");
 
const router = express.Router();

const {
    getAllDrinkMiddleware,
    createDrinkMiddleware,
    getDrinkByIdMiddleware,
    deleteDrinkMiddleware,
    updateDrinkMiddleware
} = require("../middlewares/drink.middleware")

const {
    createDrink,
    getAllDrinks,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
} = require("../controllers/drink.controller")

router.get("/", getAllDrinkMiddleware, getAllDrinks);

router.post("/", createDrinkMiddleware, createDrink)

router.get("/:drinkid", getDrinkByIdMiddleware, getDrinkById);

router.put("/:drinkid", updateDrinkMiddleware, updateDrinkById);

router.delete("/:drinkid", deleteDrinkMiddleware, deleteDrinkById);

module.exports = router;