const express = require("express");
 
const router = express.Router();

const {
    getAllVoucherMiddleware,
    createVoucherMiddleware,
    getVoucherByIdMiddleware,
    deleteVoucherMiddleware,
    updateVoucherMiddleware
} = require("../middlewares/voucher.middleware")

const {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
} = require("../controllers/voucher.controller")

router.get("/", getAllVoucherMiddleware, getAllVouchers);

router.post("/", createVoucherMiddleware, createVoucher)

router.get("/:voucherid", getVoucherByIdMiddleware, getVoucherById);

router.put("/:voucherid", updateVoucherMiddleware, updateVoucherById);

router.delete("/:voucherid", deleteVoucherMiddleware, deleteVoucherById);

module.exports = router;