const express = require("express");
 
const router = express.Router();

const {
    getAllOrderMiddleware,
    createOrderMiddleware,
    getOrderByIdMiddleware,
    deleteOrderMiddleware,
    updateOrderMiddleware
} = require("../middlewares/order.middleware")

const {
    createDetailedOrder,
    getAllOrders,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    createUserOrder
} = require("../controllers/order.controller")

router.get("/orders", getAllOrderMiddleware, getAllOrders);

router.post("/detailed-orders", createOrderMiddleware, createDetailedOrder);

router.post("/user-orders", createOrderMiddleware, createUserOrder);

router.get("/orders/:orderid", getOrderByIdMiddleware, getOrderById);

router.put("/orders/:orderid", updateOrderMiddleware, updateOrderById);

router.delete("/orders/:orderid", deleteOrderMiddleware, deleteOrderById);

module.exports = router;