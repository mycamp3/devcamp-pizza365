const express = require("express");
const router = express.Router();

const {
    createUser,
    getAllUsers,
    getAllUsersLimit,
    getAllUsersSkip,
    getAllUsersSorted,
    getAllUsersSkipLimit,
    getAllUsersSortedSkipLimit,
    getUserById,
    updateUserById,
    deleteUserById
} = require("../controllers/user.controller");

router.get("/", getAllUsers);
router.get("/limit-users", getAllUsersLimit);
router.get("/skip-users", getAllUsersSkip);
router.get("/sort-users", getAllUsersSorted);
router.get("/skip-limit-users", getAllUsersSkipLimit);
router.get("/sort-skip-limit-users", getAllUsersSortedSkipLimit);

router.post("/", createUser);
router.get("/:userid", getUserById);
router.put("/:userid", updateUserById);
router.delete("/:userid", deleteUserById);

module.exports = router;
